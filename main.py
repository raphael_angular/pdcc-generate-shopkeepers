import json
import random
import requests
from pycpfcnpj import gen
from model import shopkeeper

"""
    Important fields:
        name, surname, email, tradeName,
        companyName, cnpj,
        minimumOrder (random number between 100 and 600),
        paymentSettings (random number between 0 and 5.2)
"""

URL = "http://pdccdev.angularlabs.com.br/api/shopkeepers/"

def create_shopkeeper(path, size):
    with open(path) as data_file:
        data = json.load(data_file)
        user = shopkeeper.Shopkeeper(data)

        for i in range(size):
            i += 1
            user.name = gen_name(i)
            user.surname = gen_name(i)
            user.email = gen_email(i)
            user.tradeName = gen_name(i)
            user.companyName = gen_name(i)
            user.cnpj = gen.cnpj()
            user.minimumOrder = get_random_minimum_order()
            for j in range(3):
                user.paymentSettings[j]['percentage'] = get_random_discount()
            print(user)
            register(user)


def get_random_minimum_order():
    return random.randint(100,600)

def get_random_discount():
    return "%.2f" % random.uniform(0, 5.2)

def gen_name(n):
    return "Lojista " + str(n)

def gen_email(n):
    return "lojista" + str(n) + "@email.com"

def register(user):
    headers = {'Content-type': 'application/json'}
    data = json.dumps(user, default=lambda o: o.__dict__)
    req = requests.post(URL, data, headers=headers)
    print(req.status_code)


create_shopkeeper("data/model.json", 15)